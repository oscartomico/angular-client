import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

import { Country } from './country/country';


@Injectable({
  providedIn: 'root'
})
export class CountryService {

  constructor(private http: HttpClient) { }

  getCountries() {
    return this.http.get('http://localhost:8080/countries').toPromise();
  }

  newCountry(country: Country): Observable<any>{
    let json = JSON.stringify(country);
    let params = "json="+json;
    let headers = new HttpHeaders().set('Content-Type','application/x-www-form-urlencoded');
     
    return this.http.post('http://localhost:8080/countries/new', params, {headers: headers});
  }

  removeCountry(id: number) {
    this.http.delete('http://localhost:8080/countries/' + id)
    .subscribe(
      result => console.log(result),
      err => console.error(err)
    );
  }

  editCountry(id: number, countryData: Country) {
    console.log("EDIT " + id + countryData.name);
    return this.http.put('http://localhost:8080/countries/' + id, JSON.stringify(countryData));
  }
}
