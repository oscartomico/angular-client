import { Component, OnInit } from '@angular/core';
import { CountryService } from '../country.service';
import { Country } from './country';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-country',
  templateUrl: './country.component.html',
  styleUrls: ['./country.component.css']
})
export class CountryComponent implements OnInit {
  countries: Country[];
  country: Country;
  constructor(private countrySrv: CountryService, private route: ActivatedRoute) {
    this.countrySrv.getCountries()
    .then((d: Country[]) => {
      this.countries = d;
    })
    this.route.params.subscribe( params => this.country = params['id']);
  }

  ngOnInit() {
  }

}
