import { Component, OnInit } from '@angular/core';
import { CountryService } from '../country.service';
import { ActivatedRoute } from '@angular/router';
import { Country } from '../country/country';

import {Router} from "@angular/router"; // for back to "/"

@Component({
  selector: 'app-country-remove',
  templateUrl: './country-remove.component.html',
  styleUrls: ['./country-remove.component.css']
})
export class CountryRemoveComponent implements OnInit {
  country: Country;
  id: number;
  constructor(private countrySrv: CountryService, private route: ActivatedRoute, private router: Router) {
    this.route.params.subscribe(params => {
      this.id = params['id'];
    });
    this.countrySrv.removeCountry(this.id);
    this.countrySrv.getCountries();
    this.router.navigate(['']);
  }

  

  ngOnInit() {
  }

}
