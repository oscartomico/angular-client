import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { CountryComponent } from './country/country.component';
import { CountryService } from './country.service';
import { Routes, RouterModule } from '@angular/router';
import { CountryRemoveComponent } from './country-remove/country-remove.component';
import { CountryEditComponent } from './country-edit/country-edit.component';
import { CountryNewComponent } from './country-new/country-new.component';
import { CountrynewFormComponent } from './countrynew-form/countrynew-form.component';

import { FormsModule }   from '@angular/forms';

const rutas: Routes = [
  { path: '', component: CountryComponent },
  { path: 'countries/remove/:id', component: CountryRemoveComponent },
  { path: 'countries/edit/:id', component: CountryEditComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    CountryComponent,
    CountryRemoveComponent,
    CountryEditComponent,
    CountryNewComponent,
    CountrynewFormComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot(rutas),
    FormsModule
  ],
  providers: [CountryService],
  bootstrap: [AppComponent]
})
export class AppModule { }
