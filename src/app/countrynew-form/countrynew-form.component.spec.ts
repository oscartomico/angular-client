import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CountrynewFormComponent } from './countrynew-form.component';

describe('CountrynewFormComponent', () => {
  let component: CountrynewFormComponent;
  let fixture: ComponentFixture<CountrynewFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CountrynewFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CountrynewFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
