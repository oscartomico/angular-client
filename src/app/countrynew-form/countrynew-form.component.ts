import { Component, OnInit } from '@angular/core';
//componentes de form
import {FormControl, Validators} from '@angular/forms';
//modelo creado
import { Country } from '../country/country';
//services en angular
import { CountryService } from '../country.service';
//libreria rxjs
import { Observable } from 'rxjs';
import { of } from 'rxjs';
// Esta linea fue agregada automaticamente pueden borrarlo
import { NgForm } from '@angular/forms/src/directives/ng_form';


@Component({
  selector: 'app-countrynew-form',
  templateUrl: './countrynew-form.component.html',
  styleUrls: ['./countrynew-form.component.css']
  
})
export class CountrynewFormComponent implements OnInit {
  //ContactModel = new ContactForm();
  private emailResponse;
  private truefalse:boolean = false;
  


  //constructor(private sendServices: SendEmailService, public snackBar: MatSnackBar) { }

  constructor(private countrySrv: CountryService) { }

  ngOnInit() {
  }

onSubmit(f: NgForm){
  console.log("LLamamos al SUBMIT " + f.name);
    //this.getSentServices(this.ContactModel, f);
  //this.countrySrv.newCountry
}


}
