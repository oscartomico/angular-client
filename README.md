============= INSTALL ANGULAR CLIENT =============

This guide was writen thinking in Ubuntu 18.04

= Technologies used
* Angular
* Bootstrapp

= Requeriments 
* Angular >= 6.2.1 
* NodeJS >= 8.10.0 
* npm >= 3.5.2

= Installation steps 
== Install NodeJS and npm

Type in command line: 
sudo apt install nodejs

Then you have to install npm typing in command line 
sudo apt install npm

== Install Angular 
You have to type in command line: 
sudo npm install -g @angular/cli

= Run the project Yoy can run the project with the next command (in the project folder): 
ng serve 
or 
ng serve --open (if you want auto open web browser with the root project view)